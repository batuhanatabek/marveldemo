//
//  ComicsModel.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 1.02.2022.
//

import Foundation

struct ComicInfo: Codable {
    let code:Int
    let status:String
    let data:ComicData
}

struct ComicData: Codable {
    let offset:Int
    let limit:Int
    let total:Int
    let count:Int
    let results:[ComicsModel]
}

struct ComicsModel: Codable {
    let title:String
    let thumbnail:Thumbnail
    let dates: [ComicsDates]
}

struct ComicsDates: Codable {
    let type:String
    let date:String
}
