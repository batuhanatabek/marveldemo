//
//  MarvelModel.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 31.01.2022.
//

import Foundation

struct MarvelInfo: Codable {
    let code:Int
    let status:String
    let data:MarvelData
}

struct MarvelData: Codable {
    let offset:Int
    let limit:Int
    let total:Int
    let count:Int
    let results:[Hero]
}

struct Hero: Codable {
    let id:Int
    let name:String
    let description:String
    let thumbnail:Thumbnail
    let urls: [HeroURL]
    let comics: Comics
}

struct Comics: Codable {
    let available:Int
    let collectionURI:String
    let items: [Items]
    let returned: Int
}

struct Items: Codable {
    let resourceURI: String
    let name: String
}


struct Thumbnail: Codable {
    let path: String
    let ext: String
    
    var url: String {
        return path + "." + ext
    }
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}

struct HeroURL:Codable {
    let type:String
    let url:String
}
