//
//  Service.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 31.01.2022.
//

import Foundation
import Alamofire
import UIKit
import SwiftHash

class Service {
    func loadHeroes(page:Int = 0 ,callback:@escaping(MarvelInfo?) -> Void){
        let limit = 30
        let offset = page * limit
        let url = Constant.baseURL + "offset=\(offset)&limit=\(limit)&" + getCredentials()
        AF.request(url).responseJSON { (response) in
                    let decode: JSONDecoder = JSONDecoder()
                    guard let data = response.data,
                          let marvelInfo = try? decode.decode(MarvelInfo.self, from: data),
                        marvelInfo.code == 200 else {
                            callback(nil)
                            return
                    }
                    callback(marvelInfo)
                }
    }
    
    func loadHeroComics(id: Int , callback:@escaping(ComicData?) -> Void) {
        let url = "https://gateway.marvel.com/v1/public/characters/\(id)/comics?" + getCredentials()
        AF.request(url).responseJSON { response in
            let decode: JSONDecoder = JSONDecoder()
            guard let data = response.data ,
                  let comicInfo = try? decode.decode(ComicInfo.self, from: data),
                  comicInfo.code == 200 else {
                      callback(nil)
                      return
                  }
            callback(comicInfo.data)
        }
    }
    
    func getCredentials() -> String {
        let hash = MD5(Constant.ts+Constant.privateKey+Constant.publicKey).lowercased()
        return "ts=\(Constant.ts)&apikey=\(Constant.publicKey)&hash=\(hash)"
        }
}
