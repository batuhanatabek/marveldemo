//
//  Extension.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 31.01.2022.
//

import Foundation
import UIKit

extension UITableView {
    func createRegister(nibName: String, identifier: String) {
            register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: identifier)
        }
}
