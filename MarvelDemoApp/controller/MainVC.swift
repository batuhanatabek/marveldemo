//
//  MainVC.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 31.01.2022.
//

import UIKit
import SDWebImage

class MainVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    var characterArray = [Hero]()
    var offSet = 0
    var total = 0
    
    let service = Service()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        getData()
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail"{
            if let vc = segue.destination as? DetailVC , let index = mainTableView.indexPathForSelectedRow {
                vc.loadData(characterData: characterArray[index.row])
            }
        }
    }
    
    func getData() {
        service.loadHeroes(page: offSet) { info in
            if let info = info {
                self.characterArray += info.data.results
                self.total = info.data.total
            }
            DispatchQueue.main.async {
                self.mainTableView.reloadData()
            }
        }
    }
    
    func configureUI() {
        mainTableView.createRegister(nibName: "MainCell", identifier: "mainCell")
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.red]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}

extension MainVC: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mainTableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! MainCell
        let data = characterArray[indexPath.row]
        cell.loadData(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == characterArray.count - 10 && characterArray.count != total {
            offSet += 1
            getData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let character = characterArray[indexPath.row]
        performSegue(withIdentifier: "toDetail", sender: character)
        mainTableView.deselectRow(at: indexPath, animated: true)
    }
}
