//
//  DetailVC.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 31.01.2022.
//

import UIKit
import SDWebImage

class DetailVC: UIViewController {
    @IBOutlet weak var comicsTableView: UITableView!
    @IBOutlet weak var characterDescription: UILabel!
    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterImage: UIImageView!
    var comicArray = [ComicsModel]()
    var tempArray = [ComicsModel]()
    var count = 0
    
    let service = Service()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    func configureUI() {
        comicsTableView.createRegister(nibName: "DetailCell", identifier: "detailCell")
        characterImage.layer.cornerRadius = characterImage.frame.size.height / 2
        characterImage.layer.borderColor = UIColor.red.cgColor
        characterImage.layer.borderWidth = 1
    }
    
    func loadData(characterData: Hero ) {
        DispatchQueue.main.async { [self] in
            
            characterName.text = characterData.name
            characterImage.sd_setImage(with: URL(string: characterData.thumbnail.url))
            if characterData.description == "" {
                characterDescription.text = "No info"
            } else {
                characterDescription.text = characterData.description
            }
            getComics(heroId: characterData.id)
            
        }
    }
    
    func getComics(heroId: Int) {
        service.loadHeroComics(id: heroId) { [self] comicData in
            if let data = comicData{
                tempArray = data.results
                for index in tempArray {
                    let comicDate = Int(dateFormatter(s: index.dates[0].date))
                    let result = comicDate ?? 0 - 2005
                    if (result > 0 && count < 10){
                        comicArray.append(index)
                        count += 1
                    }
                }
            }
            DispatchQueue.main.async {
                self.comicsTableView.reloadData()
            }
        }
    }
    
    func dateFormatter(s:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "YYYY"

        if let date = dateFormatterGet.date(from: s) {
            return (dateFormatterPrint.string(from: date))
        } else {
           return ("There was an error decoding the string")
        }
    }
}

extension DetailVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comicArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = comicsTableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailCell
        let data = comicArray[indexPath.row]
        cell.loadData(data: data)
        return cell
    }
}
