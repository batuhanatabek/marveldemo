//
//  DetailCell.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 1.02.2022.
//

import UIKit
import SDWebImage

class DetailCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var comicName: UILabel!
    @IBOutlet weak var comicImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }

    func configureUI() {
        backView.layer.borderWidth = 1
        backView.layer.borderColor = UIColor.red.cgColor
        backView.layer.cornerRadius = 10
        comicImage.layer.cornerRadius = comicImage.frame.size.height / 2
        comicImage.layer.borderColor = UIColor.white.cgColor
        comicImage.layer.borderWidth = 1
    }
    
    func loadData(data: ComicsModel) {
        comicName.text = data.title
        comicImage.sd_setImage(with: URL(string: data.thumbnail.url))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
