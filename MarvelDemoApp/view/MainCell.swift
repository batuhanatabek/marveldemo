//
//  MainCell.swift
//  MarvelDemoApp
//
//  Created by Batuhan Atabek on 31.01.2022.
//

import UIKit
import SDWebImage

class MainCell: UITableViewCell {

    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    func loadData(data: Hero) {
        self.characterName.text = data.name
        if let url = URL(string: data.thumbnail.url) {
            characterImage.sd_setImage(with: url)
        } else {
            characterImage.image = nil
        }
    }
    
    func configureUI() {
        backView.layer.borderWidth = 1
        backView.layer.borderColor = UIColor.red.cgColor
        backView.layer.cornerRadius = 10
        characterImage.layer.cornerRadius = characterImage.frame.size.height / 2
        characterImage.layer.borderColor = UIColor.red.cgColor
        characterImage.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
